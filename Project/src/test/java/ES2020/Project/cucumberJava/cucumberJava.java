package ES2020.Project.cucumberJava;

//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import static org.junit.Assert.*;

public class cucumberJava { 
   
   // WebDriver driver = null; 
	
   // @Given("^A living room with more than 1 person$") 
   // public void thereArePersons() { 
   //    driver = new FirefoxDriver(); 
   //    driver.navigate().to("http://192.168.160.87:23001");
   //    if(driver.findElement(By.id("presence")).getText()=="TRUE") { 
   //       System.out.println("There is at least 1 person in the living room"); 
   //    } else { 
   //       System.out.println("Living room is empty"); 
   //    } 
   // } 
	
   // @When("^there is more then 19ºC$") 
   // public void moreThan26() { 
   //    if(driver.findElement(By.id("temp")).getText()=="20ºC") { 
   //       System.out.println("It's 20ºC currently"); 
   //    } else { 
   //       System.out.println("It's 19.8ºC currently"); 
   //    }  
   // } 
	
   // @Then("^The A/C should automatically turn on$") 
   // public void acOFF() { 
   //    if(driver.findElement(By.id("ac")).getText()=="OFF") { 
   //       System.out.println("Turning AC ON"); 
   //    } else { 
   //       System.out.println("AC is currently ON"); 
   //    }
   //    System.out.println("Test1 passed"); 
   //    driver.close(); 
   // } 

   // @Given("^A room with no presence that has the lights turned on$") 
   // public void noneRoimWithLightsON() { 
   //    driver = new FirefoxDriver(); 
   //    driver.navigate().to("http://192.168.160.87:23001");
   // } 
	
   // @When("^Both conditions detectable by our system$") 
   // public void detected() { 
   //    if(driver.findElement(By.id("presence")).getText()=="TRUE") { 
   //       System.out.println("There is at least 1 person in the living room");
   //       if(driver.findElement(By.id("lights")).getText()=="ON"){
   //          System.out.println("Lights are ON, everything OK!");
   //       } 
   //    } else { 
   //       System.out.println("Living room is empty");
   //       if(driver.findElement(By.id("lights")).getText()=="ON"){
   //          System.out.println("Lights are ON, need to turn them OFF!");
   //       } 
   //    }   
   // } 
	
   // @Then("^The lights should automatically turn off$") 
   // public void lightsON() { 
   //    if(driver.findElement(By.id("lights")).getText()=="ON") { 
   //       System.out.println("Keeping lights ON because there is presence!"); 
   //    } else { 
   //       System.out.println("Turning lights OFF because no presence!"); 
   //    }
   //    driver.close(); 
   // }
   
   @Given("The AC temperature is set to 20.0ºC") 
   public void acis20() { 
      System.out.println("AC is 20.0ºC");
   }
   
   @Then("The API should return 20.0") 
   public void apiReturn() { 

      Response response = RestAssured.get("http://192.168.160.87:23000/ac").andReturn();

      String json = response.getBody().asString();
      // System.out.println(json);
      assertEquals("20.0", json);
      System.out.println("TESTE PASSOU!");
   }
}