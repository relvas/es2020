package ES2020.Project.web;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ES2020.Project.GenerateData;
import ES2020.Project.MyDataContainer;
import ES2020.Project.Repo;
import ES2020.Project.Sender;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;





@RestController
@CrossOrigin
public class RestAPIController {
    private static final Logger LOG = LoggerFactory.getLogger(RestAPIController.class);
    @Autowired MyDataContainer rstContainer;
    @Autowired Repo osrsRepo;
    @Autowired GenerateData dataGenerator;
    @Autowired Sender sender; 

    @GetMapping("/api")
    public String apiDefault(){
        return rstContainer.getMyData().toString();
    }


    @GetMapping("/graph")
    public String getGraph(){
        String a = rstContainer.getMyData().getTime() + " " + String.valueOf(rstContainer.getMyData().getTemp());
        return a;
    }

    @GetMapping("/ac")
    public String getAC(){
        String b = String.valueOf(rstContainer.getMyData().getAc());
        return b;
    }

    @GetMapping("/last10")
    public String lastTen(){
        List<Double> list = osrsRepo.getlastTemps();
        String res = list.stream().map(Object::toString).collect(Collectors.joining("-"));
        
        return res;
    }

  
    @GetMapping("/acPOST")
    @ResponseBody
    public String getFoos(@RequestParam String num) {
        LOG.info("num received= ", num);
        dataGenerator.setAC(Double.parseDouble(num));
        sender.send(dataGenerator.toString());
        // rstContainer.getMyData().setAc(Double.parseDouble(num));
        // osrsRepo.save(rstContainer.getMyData());
        return num;
    }

    // @PostMapping(path = "/ac")
    // public String addMember(@RequestBody String num) {
    //     LOG.info("num received= ", num);
    //     rstContainer.getMyData().setAc(17.0);
    //     return "ALL GOOD";
    // }

}


    


