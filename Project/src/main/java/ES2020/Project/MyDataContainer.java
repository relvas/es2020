package ES2020.Project;

public class MyDataContainer {

    private MyData myData;
    
    public MyDataContainer() {
    }

    public MyData getMyData(){
        return this.myData;
    }

    public void setData(MyData myData){
        this.myData = myData;
    }

}
