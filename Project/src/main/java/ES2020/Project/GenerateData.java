package ES2020.Project;


import java.util.Random;

import org.json.JSONObject;


/**
 * Dummy data generator. Used to public dummy sensor data directly into kafka.
 */

public class GenerateData {

    private String name = "foo";
    private Double ac, temp;
    private Boolean motionDetector;
    private int[] possibleAcTemps = {0, 16, 17, 18, 19, 20, 21, 22, 23, 24};
    private String time = "10:00";

    public GenerateData(double ac, boolean motionDetector){
        this.ac=this.temp=ac;
        this.motionDetector=motionDetector;
    }

    public void setTime(String time){
        this.time=time;
    }

    public String getTime(){
        return time;
    }

    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
    }

    public void setAC(Double ac){
        this.ac = ac;
    }

    public double getAC(){
        return this.ac;
    }

    public void generateTemp(){
        Random rnd = new Random();
        int acTemp = possibleAcTemps[rnd.nextInt(possibleAcTemps.length)];
        
        if(ac!=0){
            if(ac>temp){
                temp+=ac/100;
            }else{
                temp-=ac/100;
            }
        }else{
            if(acTemp!=0){
                if(acTemp>temp)
                    temp+=Double.valueOf(acTemp)/100;
                else
                    temp-=Double.valueOf(acTemp)/100;
            }
        }
      
    }
    public double getTemp(){
        return this.temp;
    }
    
    public void setMotionDetector(Boolean state){
        this.motionDetector = state;
    }

    public Boolean getMotionDetector(){
        return this.motionDetector;
    }


    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("ac", ac);
        jsonObject.put("motionDetector", motionDetector);
        jsonObject.put("temp", temp);
        jsonObject.put("time", time);

        return jsonObject.toString();
    }
}
