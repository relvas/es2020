package ES2020.Project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate; /* added */

import java.util.Random;

import org.springframework.boot.web.client.RestTemplateBuilder; /* added */
import org.springframework.context.annotation.Bean; /* added */

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.beans.factory.annotation.Autowired;
@SpringBootApplication
@EnableScheduling

public class DomoticsApplication {
	
	@Autowired private Sender sender; 
	@Autowired private GenerateData dataGenerator;
	@Autowired private Random rnd;
	@Autowired Repo osrsRepo;
	
	
	public static void main(String[] args) {
		SpringApplication.run(DomoticsApplication.class, args);
	}

	@Bean @Autowired
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean public Random newRnd(){
		return new Random();
	}

	@Bean
	public GenerateData genData(){
		return new GenerateData(20.0, false);
	}
	@Bean MyDataContainer myDataContainer(){
		return new MyDataContainer();
	}


	@Scheduled(fixedDelay = 1000)
	public void newDummyData(){
		
		dataGenerator.generateTemp();
		dataGenerator.setMotionDetector(rnd.nextFloat()>0.90?true:false);
		sender.send(dataGenerator.toString());
	}
	

	
}

