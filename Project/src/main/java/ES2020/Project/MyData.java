package ES2020.Project;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.json.*;


@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class MyData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
    }

    
    private String name;
    private double temp;
    private double ac;
    private boolean motionDetector;
    private String time;
    
    public MyData(String str) {
    
        Object obj = new JSONObject(str);
        JSONObject jo = (JSONObject) obj;
        // JSONParser parser = new JSONParser();
        // JSONObject jo = (JSONObject) parser.parse(str);
        this.name = jo.getString("name");
        this.ac = jo.getDouble("ac");
        this.motionDetector = jo.getBoolean("motionDetector");
        this.temp = jo.getDouble("temp");
        this.time = jo.getString("time");


    }

    public void setTime(String time){
        this.time=time;
    }

    public String getTime(){
        return time;
    }
   
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setAc(double ac) {
        this.ac = ac;
    }

    public double getAc() {
        return this.ac;
    }
    public void setTemp(double temp) {
        this.temp = temp;
    }

    public double getTemp() {
        return this.temp;
    }
    public void setMotionDetector(boolean motionDetector) {
        this.motionDetector = motionDetector;
    }

    public boolean getMotionDetector() {
        return this.motionDetector;
    }

    

    @Override
    public String toString() {
        return "MyData{" +
                "id:'"+ id+ '\''+
                ", name:'" + name +
                 '\'' +
                 ", ac:'" + ac +
                 '\'' +
                 ", temp:'" + temp +
                 '\'' +
                 ", motionDetector:'" + motionDetector +
                 '\'' +
                 ", time:'" + time +
                 '\'' +
                '}';
    }
}
