package ES2020.Project;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class Receiver {

    private static final Logger LOG = LoggerFactory.getLogger(Receiver.class);
    @Autowired MyDataContainer myDataContainer;
    @Autowired Repo osrsRepo;
    @KafkaListener(topics = "${app.topic.foo}")
    public void listen(@Payload String message) {
        
        LOG.info("received message='{}'", message);
        
        MyData newData = new MyData(message);
		
		myDataContainer.setData(newData);
		//persist newData
		osrsRepo.save(newData);
        myDataContainer.setData(newData);
    }

}
