package ES2020.Project;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Repo extends CrudRepository<MyData, Integer> {
    @Query(value = "SELECT temp FROM my_data ORDER BY ID DESC LIMIT 10", nativeQuery = true)
    List<Double> getlastTemps();

}
